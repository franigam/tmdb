package com.franigam.tmdb;

import com.franigam.tmdb.apis.TmdbApiService;
import com.franigam.tmdb.models.Movie;
import com.franigam.tmdb.models.MovieResponse;

import org.junit.Test;
import org.mockito.ArgumentMatchers;
import org.mockito.Mockito;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;

import retrofit2.Call;

import static org.junit.Assert.*;
import static org.mockito.Mockito.*;

/**
 * Example local unit test, which will execute on the development machine (host).
 *
 * @see <a href="http://d.android.com/tools/testing">Testing documentation</a>
 */
public class TmdbUnitTest {

    @Test
    public void addition_isCorrect() throws Exception {
        assertEquals(4, 2 + 2);
    }

    @Test
    public void mock1() throws Exception {
        // mock creation
        List<Movie> mockedList = mock(List.class);

        Movie movie1 = new Movie(6638, 198663, false, 7.0, "The Maze Runner", 445.890202, "/coss7RgL0NH6g4fC2s5atvf3dFO.jpg", "en", "The Maze Runner", Arrays.asList(28, 9648, 878, 53) , "/lkOZcsXcOLZYeJ2YxJd3vSldvU4.jpg", false, "Set in a post-apocalyptic world...", "2014-09-10");
        // using mock object - it does not throw any "unexpected interaction" exception
        mockedList.add(movie1);
        mockedList.clear();

        // selective, explicit, highly readable verification
        verify(mockedList).add(movie1);
        verify(mockedList).clear();
    }

    @Test
    public void mock2() throws Exception {
        // you can mock concrete classes, not only interfaces
        LinkedList mockedList = mock(LinkedList.class);

        // stubbing appears before the actual execution
        when(mockedList.get(0)).thenReturn("first");

        // the following prints "first"
        System.out.println(mockedList.get(0));

        // the following prints "null" because get(999) was not stubbed
        System.out.println(mockedList.get(999));
    }

    @Test
    public void testMoreThanOneReturnValue()  {
        Iterator<String> i= mock(Iterator.class);
        when(i.next()).thenReturn("Mockito").thenReturn("rocks");
        String result= i.next()+" "+i.next();
        //assert
        assertEquals("Mockito rocks", result);
    }

    @Test
    public void testReturnValueDependentOnMethodParameter()  {
        Comparable<String> c= mock(Comparable.class);
        when(c.compareTo("Mockito")).thenReturn(1);
        when(c.compareTo("Eclipse")).thenReturn(2);
        //assert
        assertEquals(1, c.compareTo("Mockito"));
    }


    @Test
    public void testVerify()  {
        // create and configure mock
        Movie test = Mockito.mock(Movie.class);
        when(test.getId()).thenReturn(43);


        // call method testing on the mock with parameter 12
        test.setId(12);
        test.getId();
        test.getId();

        assertEquals(test.getId().intValue(), 43);
    }

    @Test
    public void testRequest() {
        TmdbApiService tmdbApiService = new TmdbApiService() {

            @Override
            public Call<MovieResponse> getMostPopularMovies(String apiKey, String page, String language, String region) {
                Call<MovieResponse> callPopular = this.getMostPopularMovies(apiKey, page, language, region);
                return callPopular;
            }

            @Override
            public Call<MovieResponse> getSearchMovie(String apiKey, String query, String page, String language, String region) {
                Call<MovieResponse> callSearch = this.getSearchMovie(apiKey, query, String.valueOf(page), language, region);
                return callSearch;
            }
        };

        MovieResponse movieResponse = mock(MovieResponse.class);

        when(movieResponse.getResults()).thenReturn(new ArrayList<Movie>());

    }

}