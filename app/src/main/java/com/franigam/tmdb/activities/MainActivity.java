package com.franigam.tmdb.activities;

import android.content.Context;
import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.widget.Toast;

import com.franigam.tmdb.R;
import com.franigam.tmdb.adapters.MovieAdapter;
import com.franigam.tmdb.apis.TmdbApiService;
import com.franigam.tmdb.models.Movie;
import com.franigam.tmdb.models.MovieResponse;

import java.util.ArrayList;
import java.util.List;
import java.util.Locale;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

import com.crashlytics.android.Crashlytics;
import io.fabric.sdk.android.Fabric;

public class MainActivity extends AppCompatActivity {

    public static final String TAG = "MainActivity";
    private static Retrofit retrofit = null;

    private Context mContext = this;
    private RecyclerView recyclerView = null;
    private List<Movie> movies = new ArrayList<>();
    private int currentPage = 0;


    private Retrofit getRetrofit() {
        if (retrofit == null) {
            retrofit = new Retrofit.Builder()
                    .baseUrl(mContext.getString(R.string.tmdb_api_url))
                    .addConverterFactory(GsonConverterFactory.create())
                    .build();
        }
        return retrofit;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Fabric.with(mContext, new Crashlytics()); // Crashlytics start
        setContentView(R.layout.activity_main);

        setRecyclerView();
        getPopularMovies(++currentPage);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.action_search:
                return startSearchActivity();
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    private boolean startSearchActivity() {
        Intent intent = new Intent(mContext, SearchActivity.class);
        startActivity(intent);
        return true;
    }

    private void setRecyclerView() {
        recyclerView = findViewById(R.id.popular_list);
        recyclerView.setHasFixedSize(true);
        recyclerView.setLayoutManager(new LinearLayoutManager(mContext));
        recyclerView.addOnScrollListener(getOnScrollListener());
    }

    // Check is the list be in the last position
    private RecyclerView.OnScrollListener getOnScrollListener() {
        return new RecyclerView.OnScrollListener() {

            @Override
            public void onScrolled(RecyclerView recyclerView, int dx, int dy) {
                super.onScrolled(recyclerView, dx, dy);
                LinearLayoutManager linearLayoutManager = (LinearLayoutManager) recyclerView.getLayoutManager();
                int pos = linearLayoutManager.findLastCompletelyVisibleItemPosition();
                int numItems = recyclerView.getAdapter().getItemCount();
                if (pos >= numItems - 1) {
                    Log.e(TAG, "end");
                    getPopularMovies(++currentPage);
                }
            }
        };
    }

    private void getPopularMovies(int page) {
        Log.d(TAG, "page:" + page);
        if (page >= 0) {
            getRetrofit();
            TmdbApiService tmdbApiService = retrofit.create(TmdbApiService.class);
            String apiKey = mContext.getString(R.string.tmdb_api_key);
            String region = Locale.getDefault().getCountry();
            String language = Locale.getDefault().getLanguage() + "-" + region;
            Log.d(TAG, "language: " + language);
            Call<MovieResponse> call = tmdbApiService.getMostPopularMovies(apiKey, String.valueOf(page), language, region);
            call.enqueue(getCallback());
        }
    }

    // Data request with Retrofit2
    private Callback<MovieResponse> getCallback() {
        return new Callback<MovieResponse>() {

            @Override
            public void onResponse(Call<com.franigam.tmdb.models.MovieResponse> call, Response<com.franigam.tmdb.models.MovieResponse> response) {
                if (response.body() != null && response.body().getResults().size() > 0) {
                    if (movies.isEmpty()) {
                        movies = response.body().getResults();
                        recyclerView.setAdapter(new MovieAdapter(mContext, movies, R.layout.movie_list_view));
                    } else {
                        MovieAdapter movieAdapter = (MovieAdapter) recyclerView.getAdapter();
                        movieAdapter.insert(response.body().getResults());
                        movieAdapter.notifyDataSetChanged();
                    }
                    Log.d(TAG, "Popular Movies: " + movies.size());
                }
            }

            @Override
            public void onFailure(Call<com.franigam.tmdb.models.MovieResponse> call, Throwable t) {
                Log.e(TAG, "xxx" + t.toString());
                Toast.makeText(mContext, mContext.getString(R.string.not_available), Toast.LENGTH_SHORT).show();
            }

        };
    }

}
