package com.franigam.tmdb.activities;

import android.content.Context;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.franigam.tmdb.R;
import com.franigam.tmdb.adapters.MovieAdapter;
import com.franigam.tmdb.apis.TmdbApiService;
import com.franigam.tmdb.models.Movie;
import com.franigam.tmdb.models.MovieResponse;

import java.util.ArrayList;
import java.util.List;
import java.util.Locale;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class SearchActivity extends AppCompatActivity {

    public static final String TAG = "SearchActivity";
    private static Retrofit retrofit = null;

    private Context mContext = this;
    private RecyclerView recyclerView = null;
    private EditText searchEditText = null;
    private Button deleteButton = null;
    private List<Movie> movies = new ArrayList<>();
    private int currentPage = 0;


    private Retrofit getRetrofit() {
        if (retrofit == null) {
            retrofit = new Retrofit.Builder()
                    .baseUrl(mContext.getString(R.string.tmdb_api_url))
                    .addConverterFactory(GsonConverterFactory.create())
                    .build();
        }
        return retrofit;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_search);

        setRecyclerView();
        setInterface();
    }

    private void setRecyclerView() {
        recyclerView = findViewById(R.id.search_list);
        recyclerView.setHasFixedSize(true);
        recyclerView.setLayoutManager(new LinearLayoutManager(mContext));
        recyclerView.addOnScrollListener(getOnScrollListener());
    }

    private void setInterface() {
        searchEditText = findViewById(R.id.searchEditText);
        deleteButton = findViewById(R.id.deleteButton);
        searchEditText.addTextChangedListener(getTextWatcher());
        deleteButton.setOnClickListener(getDeleteOnClickListener());
    }

    // Trigger the data request if the text change
    private TextWatcher getTextWatcher() {
        return new TextWatcher() {

            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {

                String text = charSequence.toString();
                Log.d(TAG, "onTextChanged: " + text);
                if (!text.isEmpty()) {
                    currentPage = 0;
                    searchMovie(charSequence.toString(), ++currentPage);
                } else {
                    clearMovieAdapter();
                }
            }

            @Override
            public void afterTextChanged(Editable editable) {

            }
        };
    }

    private View.OnClickListener getDeleteOnClickListener() {
        return new View.OnClickListener() {
            public void onClick(View v) {
                searchEditText.setText("");
                clearMovieAdapter();
            }
        };
    }

    private void clearMovieAdapter() {
        MovieAdapter movieAdapter = (MovieAdapter) recyclerView.getAdapter();
        if (movieAdapter != null) {
            currentPage = 0;
            movieAdapter.clearAll();
            movieAdapter.notifyDataSetChanged();
        }
    }

    // Check is the list be in the last position
    private RecyclerView.OnScrollListener getOnScrollListener() {
        return new RecyclerView.OnScrollListener() {

            @Override
            public void onScrolled(RecyclerView recyclerView, int dx, int dy) {
                super.onScrolled(recyclerView, dx, dy);
                LinearLayoutManager linearLayoutManager = (LinearLayoutManager) recyclerView.getLayoutManager();
                int pos = linearLayoutManager.findLastCompletelyVisibleItemPosition();
                int numItems = recyclerView.getAdapter().getItemCount();
                if (pos >= numItems - 1) {
                    Log.d(TAG, "End List");
                    if (searchEditText.getText().toString() != "") {
                        searchMovie(searchEditText.getText().toString(), ++currentPage);
                    }
                }
            }
        };
    }

    private void searchMovie(String query, int page) {
        Log.d(TAG, "page:" + page);
        if (!query.isEmpty() && page >= 0) {
            getRetrofit();
            TmdbApiService tmdbApiService = retrofit.create(TmdbApiService.class);
            String apiKey = mContext.getString(R.string.tmdb_api_key);
            String region = Locale.getDefault().getCountry();
            String language = Locale.getDefault().getLanguage() + "-" + region;
            Log.d(TAG, "language:" + language);
            Call<MovieResponse> call = tmdbApiService.getSearchMovie(apiKey, query, String.valueOf(page), language, region);
            call.enqueue(getCallback());
        }
    }

    // Data request with Retrofit2
    private Callback<MovieResponse> getCallback() {
        return new Callback<MovieResponse>() {

            @Override
            public void onResponse(Call<MovieResponse> call, Response<MovieResponse> response) {
                if (response.body() != null) {
                    if (movies.isEmpty()) {
                        movies = response.body().getResults();
                        recyclerView.setAdapter(new MovieAdapter(mContext, movies, R.layout.movie_list_view));
                    } else {
                        MovieAdapter movieAdapter = (MovieAdapter) recyclerView.getAdapter();
                        if (movieAdapter != null) {
                            if (response.body().getPage() == 1) {
                                movieAdapter.clearAll();
                            }
                            movieAdapter.insert(response.body().getResults());
                            movieAdapter.notifyDataSetChanged();
                        }
                    }
                    Log.d(TAG, "Query Result: " + movies.size());
                }
            }

            @Override
            public void onFailure(Call<MovieResponse> call, Throwable t) {
                Log.e(TAG, t.toString());
                Toast.makeText(mContext, mContext.getString(R.string.not_available), Toast.LENGTH_SHORT).show();
            }

        };
    }

}
