package com.franigam.tmdb.adapters;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.util.TypedValue;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.franigam.tmdb.R;
import com.franigam.tmdb.models.Movie;
import com.squareup.picasso.Picasso;

import java.util.List;

/**
 * Created by franigam on 04/02/2018.
 */

public class MovieAdapter extends RecyclerView.Adapter<MovieAdapter.MovieViewHolder> {

    public static final String TAG = "MovieAdapter";
    private Context mContext;
    private LayoutInflater mInflater;
    private List<Movie> mDataSource;
    private int mlayout;

    public MovieAdapter(Context context, List<Movie> items, int layout) {
        mContext = context;
        mDataSource = items;
        mlayout = layout;
        mInflater = (LayoutInflater) mContext.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
    }

    @Override
    public MovieViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View rowView = mInflater.inflate(mlayout, parent, false);
        return new MovieViewHolder(rowView);
    }

    @Override
    public void onBindViewHolder(MovieViewHolder holder, int position) {
        getPosterHolder(holder, position);
        getVoteAverageHolder(holder, position);
        holder.titleTextView.setText(mDataSource.get(position).getTitle());
        String year = mDataSource.get(position).getReleaseDate();
        if (year != "" && year.length() >= 4) {
            year = year.substring(0, 4); // coge sólo el año de la fecha
        }
        holder.yearTextView.setText(year);
        String overview = mDataSource.get(position).getOverview();
        if (!overview.isEmpty()) {
            holder.overviewTextView.setText(overview);
        } else {
            holder.overviewTextView.setText(mContext.getString(R.string.not_overview));
        }
    }

    @Override
    public long getItemId(int i) {
        return i;
    }

    @Override
    public int getItemCount() {
        return mDataSource.size();
    }

    // Image request with Picasso
    private void getPosterHolder(MovieViewHolder holder, final int position) {
        String imageUrl = mContext.getString(R.string.tmdb_api_image_url) + getPosterWithOptimalWidth(100) + mDataSource.get(position).getPosterPath();
        Log.d(TAG, imageUrl);
        Picasso.with(mContext)
                .load(imageUrl)
                .error(R.drawable.not_available)
                .into(holder.posterImageView);
    }

    public void insert(List<Movie> items) {
        mDataSource.addAll(items);
    }

    public void clearAll() {
        mDataSource.clear();
    }

    // get optimal width
    private String getPosterWithOptimalWidth(int dp) {
        // dp to pixel
        int pixel = (int) TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP,
                dp, mContext.getResources().getDisplayMetrics());
        if (pixel <= 45) {
            return mContext.getString(R.string.tmdb_api_image_url_width) + 45;
        } else if (pixel <= 92) {
            return mContext.getString(R.string.tmdb_api_image_url_width) + 92;
        } else if (pixel <= 154) {
            return mContext.getString(R.string.tmdb_api_image_url_width) + 154;
        } else if (pixel <= 185) {
            return mContext.getString(R.string.tmdb_api_image_url_width) + 185;
        } else if (pixel <= 300) {
            return mContext.getString(R.string.tmdb_api_image_url_width) + 300;
        } else if (pixel <= 500) {
            return mContext.getString(R.string.tmdb_api_image_url_width) + 500;
        } else {
            return mContext.getString(R.string.original_width);
        }
    }

    // Change the colour according the Vote Average
    private void getVoteAverageHolder(MovieViewHolder holder, final int position) {
        Double voteAverage = mDataSource.get(position).getVoteAverage();
        if (voteAverage == 0) {
            holder.voteAverageTextView.setText(mContext.getString(R.string.not_rated));
            holder.voteAverageTextView.setTextColor(mContext.getResources().getColor(R.color.voteAverageNotRated));
        } else {
            holder.voteAverageTextView.setText(voteAverage.toString());
            if (voteAverage >= 7) {
                holder.voteAverageTextView.setTextColor(mContext.getResources().getColor(R.color.voteAverageGreen));
            } else if (voteAverage >= 4) {
                holder.voteAverageTextView.setTextColor(mContext.getResources().getColor(R.color.voteAverageYellow));
            } else {
                holder.voteAverageTextView.setTextColor(mContext.getResources().getColor(R.color.voteAverageRed));
            }
        }
    }

    public static class MovieViewHolder extends RecyclerView.ViewHolder {

        public ImageView posterImageView;
        public TextView voteAverageTextView;
        public TextView titleTextView;
        public TextView yearTextView;
        public TextView overviewTextView;

        public MovieViewHolder(View view) {
            super(view);
            posterImageView = view.findViewById(R.id.poster);
            voteAverageTextView = view.findViewById(R.id.vote_average);
            titleTextView = view.findViewById(R.id.tittle);
            yearTextView = view.findViewById(R.id.year);
            overviewTextView = view.findViewById(R.id.overview);
        }

    }

}
