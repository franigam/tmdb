package com.franigam.tmdb.apis;

import com.franigam.tmdb.models.MovieResponse;

import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Query;

/**
 * Created by franigam on 04/02/2018.
 */

public interface TmdbApiService {

    @GET("movie/popular")
    Call<MovieResponse> getMostPopularMovies(@Query("api_key") String apiKey, @Query("page") String page, @Query("language") String language, @Query("region") String region);

    @GET("search/movie")
    Call<MovieResponse> getSearchMovie(@Query("api_key") String apiKey, @Query("query") String query, @Query("page") String page, @Query("language") String language, @Query("region") String region);

}
